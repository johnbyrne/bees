angular.module('beesApp.constants', []).
constant('HitpointsConfig', {
  MAX_QUEEN_BEE_HP: 100,
  MAX_WORKER_BEE_HP: 75,
  MAX_DRONE_BEE_HP: 50,
});
