'use strict';

angular.module('beesApp.beesService', [])

.service('BeesService', ['HitpointsConfig', function(HitpointsConfig) {
  return {
    initQueenBee: {
      hitpoints: HitpointsConfig.MAX_QUEEN_BEE_HP,
      damageTakenPerHit: 8,
      population: 1
    },

    initWorkerBees: {
      hitpoints: HitpointsConfig.MAX_WORKER_BEE_HP,
      damageTakenPerHit: 10,
      population: 5
    },

    initDroneBees: {
      hitpoints: HitpointsConfig.MAX_DRONE_BEE_HP,
      damageTakenPerHit: 12,
      population: 8
    }
  }
}]);
