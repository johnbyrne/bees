'use strict';

// Declare app level module which depends on views, and components
angular.module('beesApp', [
  'ngRoute',
  'beesApp.constants',
  'beesApp.gameView',
  'beesApp.winView',
  'beesApp.beesService'
]).
constant('HitpointsConfig', {
  MAX_QUEEN_BEE_HP: 100,
  MAX_WORKER_BEE_HP: 75,
  MAX_DRONE_BEE_HP: 50,
}).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/game'});
}]);
