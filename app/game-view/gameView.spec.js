'use strict';

describe('beesApp.gameView module', function() {
    beforeEach(module('beesApp.gameView'));
    beforeEach(function() {
      module('beesApp.constants');
      module('beesApp.beesService');
    });
    describe('IndividualCtrl', function() {
        var scope,
          ctrl,
          service;

        beforeEach(inject(function($rootScope, $controller, BeesService) {
          scope = $rootScope.$new();
          service = BeesService;
          ctrl = $controller('GameViewCtrl', {
            $scope: scope,
            BeesService: service
          });
        }));

        it('should define the game view controller', function() {
          expect(ctrl).toBeDefined();
        });

        it('should initialise the queen bee', function() {
          scope.initBees();
          expect(scope.queenBee).toEqual({
              hitpoints: 100,
              damageTakenPerHit: 8,
              population: 1
          });
        });

        it('should initialise the worker bees', function() {
          scope.initBees();
          expect(scope.workerBees).toEqual({
              hitpoints: 75,
              damageTakenPerHit: 10,
              population: 5
          });
        });

        it('should initialise the drone bees', function() {
          scope.initBees();
          expect(scope.droneBees).toEqual({
              hitpoints: 50,
              damageTakenPerHit: 12,
              population: 8
          });
        });

        it('should remove the correct hitpoints', function() {
          scope.initBees();
          scope.attack('worker')
          expect(scope.workerBees).toEqual({
              hitpoints: 65,
              damageTakenPerHit: 10,
              population: 5
          });
        });


    });
});
