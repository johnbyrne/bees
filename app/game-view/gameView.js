'use strict';

angular.module('beesApp.gameView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/game', {
    templateUrl: 'game-view/gameView.html',
    controller: 'GameViewCtrl'
  });
}])

.controller('GameViewCtrl', ['$scope', '$location', 'BeesService', 'HitpointsConfig',
  function($scope, $location, BeesService, HitpointsConfig) {

  var QUEEN_BEE = 0,
    WORKER_BEE = 1,
    DRONE_BEE = 2;

  $scope.initBees = function() {
    $scope.queenBee = angular.copy(BeesService.initQueenBee);
    $scope.workerBees = angular.copy(BeesService.initWorkerBees);
    $scope.droneBees = angular.copy(BeesService.initDroneBees);
  }

  $scope.initBees();

  function getRandom() {
    return Math.floor(Math.random() * 3);
  }

  $scope.attack = function(beeType) {
    var random;

    switch (beeType) {
      case 'random':
        random = getRandom();
        break;
      case 'queen':
        random = QUEEN_BEE;
        break;
      case 'worker':
        random = WORKER_BEE;
        break;
      case 'drone':
        random = DRONE_BEE;
        break;
    }

    switch (random) {
      case QUEEN_BEE:
        if ($scope.queenBee.population > 0) {
          $scope.queenBee.hitpoints -= $scope.queenBee.damageTakenPerHit;
          if ($scope.queenBee.hitpoints <= 0) {
            alert('All bees dead');
            $scope.initBees();
            $location.path('/win');
          }
        }
        break;
      case WORKER_BEE:
        if ($scope.workerBees.population > 0) {
          $scope.workerBees.hitpoints -= $scope.workerBees.damageTakenPerHit;
          if ($scope.workerBees.hitpoints <= 0) {
            $scope.workerBees.population--;
            $scope.workerBees.hitpoints = HitpointsConfig.MAX_WORKER_BEE_HP;
          }
        } else {
          $scope.attack('random');
        }
        break;
      case DRONE_BEE:
       if ($scope.droneBees.population > 0) {
          $scope.droneBees.hitpoints -= $scope.droneBees.damageTakenPerHit;
          if ($scope.droneBees.hitpoints <= 0) {
            $scope.droneBees.population--;
            $scope.droneBees.hitpoints = HitpointsConfig.MAX_DRONE_BEE_HP;
          }
        } else {
          $scope.attack('random');
        }
        break;
    }
  }
}]);
