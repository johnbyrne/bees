'use strict';

angular.module('beesApp.winView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/win', {
    templateUrl: 'win-view/winView.html',
    controller: 'WinViewCtrl'
  });
}])

.controller('WinViewCtrl', ['$scope', '$location', function($scope, $location) {
  $scope.go = function (path) {
    $location.path(path);
  };
}]);
